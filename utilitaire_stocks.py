import csv
import os
import datetime
from tkinter import *
from tkinter.filedialog import askopenfilename


fenetre = Tk()


champ_label = Label(fenetre, text="Veuillez choisir un fichier")
champ_label.pack()

name = askopenfilename()
print(name) 
csv_depart = name

#recuperation de la date
date = datetime.datetime.now()
str(date)


#creation d'un nouveau fichier CSV compatible avec le logiciel de reappro
with open(csv_depart, "r") as fsrce:
    with open("fichier_stocks" + "-" + str(date.day) + "-" +str(date.month) + "-" + str(date.year) + ".csv", "w", newline='') as fdest:
        my_reader = csv.reader(fsrce, delimiter = ',')
        my_writer = csv.writer(fdest, delimiter = ',')
        for ligne in my_reader: # ligne est une liste de valeurs de colonnes
            myCsvRow = ('"' + ligne[0] + '"' + ',' + '"' + ligne[3] + '"' + ',' + '"' + ligne[4] + '"' + ',' + '"' + ligne[5] + '"' + '\n')
            fdest.write(myCsvRow)
        champ_label.pack_forget()
        texte_fin = Label(fenetre, text="Fini !")
        texte_fin.pack()


input("Press Enter to continue...")